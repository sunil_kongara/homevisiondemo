﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;

namespace HVImgDownload.Utilities
{
    public class HTTPUtility
    {
        public static byte[] DownloadImage(string imageUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(imageUrl);

            Byte[] imageBytes;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (BinaryReader reader = new BinaryReader(response.GetResponseStream()))
                {
                    imageBytes = reader.ReadBytes(1 * 1024 * 1024 * 10);
                    
                }
            }
            return imageBytes;
        }
        public static string GetData(string url,int retryCount = 0)
        {
            
            do
            {
                --retryCount;
                try
                {
                    return GetHttpResponse(url);
                }
                catch { }
            } while (retryCount > 0);
            return string.Empty;
        }

        private static string GetHttpResponse(string url)
        {
            HttpClient httpClient = new HttpClient();
            try
            {
                var response = httpClient.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
                if(response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                {
                    throw new HttpRequestException();
                }
            }
            catch (HttpRequestException requestException)
            {
                throw requestException;
            } 
            catch(Exception ex)
            {
                //log error
            }

            return string.Empty ;
            
        }
    }
}
