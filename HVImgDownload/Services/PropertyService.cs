﻿
using HVImgDownload.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace HVImgDownload.Services
{
    public class PropertyService : IPropertyService
    {
        const int TOTAL_PAGES = 10;
        const int RETRY_COUNT = 10; //Service is flaky so using retries
        const string HOUSE_URL = "http://app-homevision-staging.herokuapp.com/api_project/houses?page={0}";
        public void savePictures()
        {
            getHouseDetails();
        }

        private void getHouseDetails()
        {
            List<House> houses = new List<House>();
            
            for(int i = 0; i < TOTAL_PAGES; i++)
            {
                
                string homedata = HVImgDownload.Utilities.HTTPUtility.GetData(string.Format(HOUSE_URL, i+1),RETRY_COUNT);
                JObject json = JObject.Parse(homedata);
                JArray houseArray = (JArray)json["houses"];
                houses.AddRange(houseArray.ToObject<IList<House>>());
                
            }

            savePicturesToDrive(houses);
        }

        private void savePicturesToDrive(List<House> houses)
        {
            //Gets pictures concurrently
            Parallel.ForEach(houses, (house) =>
            {
                byte[] imageBytes = HVImgDownload.Utilities.HTTPUtility.DownloadImage(house.photoURL);
                using (FileStream fileStream = new FileStream(string.Format("id-{0}-{1}.jpg", house.price, house.address), FileMode.Create))
                {
                    fileStream.Write(imageBytes, 0, imageBytes.Length);
                }

                
            });
        }
    }
}
