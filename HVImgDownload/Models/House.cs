﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HVImgDownload.Models
{
    public class House
    {
        public int id { get; set; }
        public string address { get; set; }
        public string homeowner { get; set; }
        public long price { get; set; }
        public string photoURL { get; set; }
    }
}
